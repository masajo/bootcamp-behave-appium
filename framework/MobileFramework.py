from appium import webdriver

# Framework para trabajar con APPIUM

'''
Configuración de las ** DESIRED CAPABILITIES ** (Confifuración de conexión con el dispositivo móvil
y la aplicación que se va a probar para que APPIUM inicie sesión:

- platformName: "Android" / "IOS"
- deviceName: Nombre del dispositivo --> Al listar el dispositivo podemos ver el nombre
- app: Path/Nombre de la aplicación que vamos a probar en el dispositivo
- appPackage: 
- appWaitActivity:
- ...

'''

# TODO: Rellenar Capabilities
desired_capabilities = {
    "platformName": "Android",
    "deviceName": "XXXXXXX",
    "app": "../utilities/apks/XXXXXX.apk"
}


# Creamos la instancia del Driver que vamos a usar y la conexión remota con el dispositivo
web_driver = webdriver.Remote("http://locahost:4723/wd/hub", desired_capabilities)





